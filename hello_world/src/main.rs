#![feature(start)]
#![no_std]

#[link(name="c")]
extern "C" {
    fn printf(fmt: *const u8);
}

#[start]
fn main(_argc : isize, _argv : *const *const u8) -> isize {
    unsafe {printf(b"Hello World!\n\0" as *const u8);}
    0
}

use core::panic::PanicInfo;

#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}
