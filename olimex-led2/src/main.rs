#![no_main]
#![no_std]


// Contains all low-level unsafe code
mod peripherals {
    use core::sync::atomic::AtomicBool;
    use core::sync::atomic::Ordering;
    static PERIPHERALS_INITIALISED : AtomicBool = AtomicBool::new(false);

    const GPIOC : usize = 0x40020800;
    const GPIOC_MODE : *mut u32 = GPIOC as *mut u32;
    const GPIOC_OUTPUT_TYPE : *mut u32 = (GPIOC + 0x4) as *mut u32;
    const GPIOC_OUTPUT_SPEED: *mut u32 = (GPIOC + 0x8) as *mut u32;
    const GPIOC_OUTPUT_DATA: *mut u32 = (GPIOC + 0x14) as *mut u32;

    const RCC : usize = 0x40023800;
    const RCC_AHB1_CLOCK : *mut u32 = (RCC + 0x30) as *mut u32;

    unsafe fn modify<T : Copy, F : FnOnce(T) -> T>(t : *mut T, f : F) {
        let value = t.read_volatile();
        let result = f(value);
        t.write_volatile(result);
    }

    unsafe fn led_init() {
        modify(RCC_AHB1_CLOCK, |w| {w | 0x4});
        modify(GPIOC_MODE, |w| {w | (1 << 24)});
        modify(GPIOC_OUTPUT_SPEED, |w| {w | (3 << 24)});
        modify(GPIOC_OUTPUT_TYPE, |w| {w | (1 << 12)});
    }

    use core::marker::PhantomData;

    // Given a member to prevent external construction
    // (Second function of PhantomData)
    pub struct Led(PhantomData<()>);

    impl Led {
        pub fn new() -> Option<Self> {
            let initialised = PERIPHERALS_INITIALISED.swap(true, Ordering::SeqCst);
            if initialised {
                None
            } else {
                unsafe{
                    led_init();
                }
                Some(Self(PhantomData))
            }
        }

        // Mutable to prevent shared access
        pub fn on(&mut self) {
            unsafe{modify(GPIOC_OUTPUT_DATA, |w| {w | (1 << 12)});}
        }

        pub fn off(&mut self) {
            unsafe{modify(GPIOC_OUTPUT_DATA, |w| {w & !(1 << 12)});}
        }

        pub fn toggle(&mut self) {
            unsafe{modify(GPIOC_OUTPUT_DATA, |w| {w ^ (1 << 12)});}
        }
    }
}

fn hash_int(t : u64) -> u8 {
    let a = t as u8;
    let a = (a << 4) + (a >> 4);
    let a = ((a & 0xCC) >> 2) + ((a & 0x33) << 2);
    let a = ((a & 0xAA) >> 1) + ((a & 0x55) << 1);
    a
}

use peripherals::Led;
fn main() -> ! {
    let mut leds = Led::new().unwrap();
    let slowness = 5;
    let mut s : u64 = 1 << slowness;
    loop {
        for i in 0..s {
            let q = hash_int(i) as u64;
            if q * s * s > 255 * i * i {
                leds.on();
            } else {
                leds.off();
            }
        }
        for i in (0..s).rev() {
            let q = hash_int(i) as u64;
            if q * s * s > 255 * i * i {
                leds.on();
            } else {
                leds.off();
            }
        }
        s += s >> slowness;
    }
}

// The reset handler
#[no_mangle]
pub unsafe extern "C" fn Reset() -> ! {
    main()
}

// The reset vector, a pointer into the reset handler
#[link_section = ".vector_table.reset_vector"]
#[no_mangle]
pub static RESET_VECTOR: unsafe extern "C" fn() -> ! = Reset;

use core::panic::PanicInfo;
#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}

