\title{Rust on Embedded Systems}
\maketitle

\makeoverview

%------------------------------------------------

\section{Introduction}
\makecurrentsection

\begin{frame}
    \frametitle{\currentname}
	\pause
	THE KNOWLEDGE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF UNLEASHING
	INDESCRIBABLE HORRORS THAT SHATTER YOUR PSYCHE AND SET YOUR MIND ADRIFT IN THE UNKNOWABLY INFINITE COSMOS.
	
	\textit{- The Rustonomicon}
\end{frame}

\begin{frame}
	\frametitle{References}

	\small
	The Rustonomicon\\\quad {\small(\texttt{https://doc.rust-lang.org/nomicon/})}
	
	The Embedonomicon\\\quad {\small(\texttt{https://docs.rust-embedded.org/embedonomicon/})}

	The ANSI C Specification\\\quad {\small(\texttt{http://eli-project.sourceforge.net/c\_html/c.html})}

	The Cortex-M-RT crate\\\quad {\small(\texttt{https://github.com/rust-embedded/cortex-m-rt})}

	The ARM Instruction Set\\\quad {\small(\texttt{https://developer.arm.com/docs/})}

	How to learn Rust with entirely too many linked lists\\\quad {\small(\texttt{https://rust-unofficial.github.io/too-many-lists/})}

	\pause
	That one file from the Nalys training on an Olimex board last year.
\end{frame}

\section{Why (not) Rust?}
\makecurrentsection


\begin{frame}
    \frametitle{\currentname}
    Why Rust?
	\pause

	\begin{itemize}
		\item Strong type system
		\item Memory/concurrency related errors impossible
		\item No runtime, no garbage collection
		\item Efficient code
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\currentname}
    Rust is not always a great idea:
	\pause

	\begin{itemize}
		\item You want mature tooling
			\pause
		\item You want a stable environment
			\pause
		\item You want to use a widespread language
			\pause
		\item Your device has an AVR instruction set
	\end{itemize}
\end{frame}

\section{Hello world?}
\makecurrentsection

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Hello world in Rust}

	\begin{lstlisting}
	fn main() {
		println!("Hello world!");
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\lstinputlisting[basicstyle=\tiny]{../hello_world/src/main.rs}
\end{frame}

\begin{frame}
    \frametitle{\currentname}
	\textbf{Benefits}
	\pause

	This program no longer relies on:
	\begin{itemize}
		\item Allocations
		\item IO
		\item The entire Rust Standard Library.
	\end{itemize}

	\pause
	Requirements for embedded systems!
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\lstinputlisting[basicstyle=\tiny]{../hello_world/src/main.rs}

	Let's go through it step by step.
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	#![feature(start)]
	\end{lstlisting}
	
	Some lang items are unstable.
\end{frame}


\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	#![no_std]
	\end{lstlisting}
	
	 This also excludes vital functionality!
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	#[link(name="c")]
	extern "C" {
		fn printf(fmt: *const u8);
	}
	\end{lstlisting}
	

	\begin{itemize}
		\item Compiling on a x86\_64 GNU system requires some magic
		\item libc provides that magic
		\item Also declare printf
	\end{itemize}
\end{frame}


\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	#[start]
	\end{lstlisting}

	Marks the next function as start function.
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	fn main(_argc : isize, _argv : *const *const u8) -> isize {...}
	\end{lstlisting}
	Like \verb|int main(int argv, char ** argc)| but slightly more annoying. 

	Must be this exact type for \verb|#[start]| to work.  

	Might be changed in a future release!
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	unsafe {printf(b"Hello World!\n\0" as *const u8);}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting}
	0
	\end{lstlisting}

	The last expression in any function is its return value:

	In C:
	\begin{itemize}
		\item \verb|return 0;|
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\begin{lstlisting} 
	use core::panic::PanicInfo;

	#[panic_handler]
	fn panic(_panic: &PanicInfo<'_>) -> ! {
		loop {}
	}
	\end{lstlisting}

	Replaces exceptions
	
	However, panic handling is done in stdlib, so we have to implement our own

\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
	\textbf{Do we need all of this?}

	\pause

	We don't!

	\lstinputlisting{../hello_world/src/main2.rs}

	\small\texttt{https://gitlab.com/gdox/embedded-rust/tree/master/hello\_world}

\end{frame}

\section{Guarantees}
\makecurrentsection

\begin{frame}
    \frametitle{\currentname}
	Rust provides a few guarantees, including:
	
	\begin{itemize}
		\item No use after free
		\item No double free
		\item No data races
		\item No invalid memory read/write
	\end{itemize}

	\pause

	For this to work, Rust checks a few invariants:

	\begin{itemize}
		\item No references living after object destruction
		\item No object accessible after destruction
		\item \textbf{No shared mutable state}
		\item No arbitrary or out-of-bound memory accesses
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\currentname}
	\centering
	Small issue:

	\pause
	\textit{\Large HARDWARE == SHARED MUTABLE STATE}

	\pause
	Solution: \textit{unsafe}
\end{frame}


\section{Unsafe}
\makecurrentsection

\begin{frame}[fragile]
	\frametitle{What is unsafe?}
	Sometimes difficult to explain safety to a compiler:

	\begin{itemize}
			\pause
		\item \verb|0x40020814| is actually a memory mapped register!
			\pause
		\item \verb|printf| won't cause undefined behaviour!
			\pause
		\item \verb|matrix| has more than 4 elements, no need for bounds checking!
			\pause
		\item This doubly linked list is correctly implemented!
	\end{itemize}

	\pause
	Impossible without an escape hatch
\end{frame}

\begin{frame}[fragile]
	\frametitle{What is unsafe?}
	Unsafe lets you:

	\begin{tabular}{l l}
		\tabitem Dereference a pointer;
	&\begin{lstlisting}[basicstyle=\scriptsize]
let ptr = 0x1000 as *mut u32;
unsafe {
	let i = *ptr;
}
	\end{lstlisting}\\\\
		\tabitem Modify static variables;
	&\begin{lstlisting}[basicstyle=\scriptsize]
static mut i : i32 = 0;
unsafe {
	i += 1;
}
	\end{lstlisting}\\\\
		\tabitem Call other unsafe functions.
	&\begin{lstlisting}[basicstyle=\scriptsize]
let f : f32 = 1.0;
unsafe {
	let i : i32 = transmute(f);
}
	\end{lstlisting}
\end{tabular}

	\pause

	\normalsize
	Why is this so dangerous?
	\pause
	\textbf{Undefined Behaviour}
\end{frame}

\begin{frame}
	\frametitle{Undefined Behaviour}
	Includes:

	\begin{itemize}
		\item Use after free
		\item Double free
		\item Data race
		\item Accessing invalid/out of bound memory
		\item Modifying an immutable reference
		\item Accessing a mutable reference
		\item Having two references alias
		\item Constructing invalid values
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Undefined Behaviour}
	\textbf{What about Safe Rust?}

	\pause

	$\Rightarrow$ Guaranteed absence of UB!
\end{frame}

\section{Bare metal}
\makecurrentsection

\begin{frame}[fragile]
	\frametitle{\currentname}

	\textbf{Fairly similar to C}

	\begin{lstlisting}
	const GPIOC : usize = 0x40020800;
	const GPIOC_MODE : *mut u32 = GPIOC as *mut u32;
	const GPIOC_OUTPUT_TYPE : *mut u32 = (GPIOC + 0x4) as *mut u32;
	const GPIOC_OUTPUT_SPEED: *mut u32 = (GPIOC + 0x8) as *mut u32;
	const GPIOC_OUTPUT_DATA: *mut u32 = (GPIOC + 0x14) as *mut u32;
	\end{lstlisting}
	
\end{frame}

\begin{frame}[fragile]
	\frametitle{\currentname}

	\begin{lstlisting}
	unsafe fn set_led_output_type() {
		let mut value = GPIOC_OUTPUT_TYPE.read_volatile();
		value |= (1 << 12);
		GPIOC_OUTPUT_TYPE.write_volatile(value);
	}
	\end{lstlisting}
	
\end{frame}
%\begin{frame}[fragile]
%	\frametitle{\currentname}
%	Too repetitive?
%	\pause
%
%	\begin{lstlisting}
%	unsafe fn set(t : *mut u32, mask : u32) {
%		let mut value = t.read_volatile();
%		value |= mask;
%		t.write_volatile(value);
%	}
%	\end{lstlisting}
%	
%\end{frame}
%
%\begin{frame}[fragile]
%	\frametitle{\currentname}
%	\textbf{Even More Abstraction}
%	\begin{lstlisting}
%	unsafe fn modify<T : Copy, F : FnOnce(T) -> T>(t : *mut T, f : F) {
%		let value = t.read_volatile();
%		let result = f(value);
%		t.write_volatile(result);
%	}
%	\end{lstlisting}
%	
%\end{frame}
%
%\begin{frame}[fragile]
%	\frametitle{\currentname}
%
%	\begin{lstlisting}
%	unsafe fn led_init() {
%		modify(RCC_AHB1_CLOCK, |w| {w | 0x4});
%		modify(GPIOC_MODE, |w| {w | (1 << 24)});
%		modify(GPIOC_OUTPUT_SPEED, |w| {w | (3 << 24)});
%		modify(GPIOC_OUTPUT_TYPE, |w| {w | (1 << 12)});
%	}
%
%	unsafe fn led_blink() {
%		modify(GPIOC_OUTPUT_DATA, |w| {w ^ (1 << 12)});
%	}
%	\end{lstlisting}
%	
%\end{frame}
\begin{frame}[fragile]
	\frametitle{\currentname}

	\begin{lstlisting}
	// The reset handler
	#[no_mangle]
	pub unsafe extern "C" fn Reset() -> ! {...}

	#[link_section = ".vector_table.reset_vector"]
	#[no_mangle]
	pub static RESET_VECTOR: unsafe extern "C" fn() -> ! = Reset;
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\currentname}

	Compile with 

	\verb|cargo build --target thumbv7m-none-eabi -C link-arg=-Tlink.x|

	(Or just \verb|cargo build| with the proper configuration files)
\end{frame}


\begin{frame}[fragile]
	\frametitle{\currentname}
	\textbf{And that's it!}

	\small\texttt{https://gitlab.com/gdox/embedded-rust/tree/master/olimex-led}
\end{frame}

\section{Back to safety}
\makecurrentsection

\begin{frame}
	\frametitle{\currentname}
	\textbf{Issues?}

	\pause

	\textit{Unsafe code all around!}
	
	\pause

	Solution : \textit{encapsulation}
\end{frame}


\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 1}: Modules

	Rust has very strict module boundaries

	Example:

	\begin{lstlisting}
	pub struct Vec<T> {
		data : *mut T,
		len : usize,
		cap : usize,
	}
	\end{lstlisting}
	
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 1}: Modules

	Rust has very strict module boundaries

	Example:

	\begin{lstlisting}
	mod vector {
		pub struct Vec<T> {
			data : *mut T,
			len : usize,
			cap : usize,
		}
	}
	pub use vector::Vec;
	\end{lstlisting}

	
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 1}: Modules

	Rust has very strict module boundaries

	All code modifying cap and data inside module $<$ single file.

	$\Rightarrow$ Easier to check guarantees!
\end{frame}
\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 1}: Modules

	Rust has very strict module boundaries

	\begin{lstlisting}
	mod peripherals {
		// unsafe code
	}
	// safe code!
	\end{lstlisting}

\end{frame}
\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 2}: Ownership

	Hardware == shared mutable state.

	How does Rust prevent shared mutable state?

	\pause

	Single ownership
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 2}: Ownership

	\begin{lstlisting}
	led_on();

	// VS

	let mut led = Led::new();
	led.on();
	\end{lstlisting}

	How do we prevent multiple led instances?
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 2}: Ownership

	Through step 1!

	\begin{lstlisting}
	mod peripherals {
		static LED_EXISTS : bool = false;
		struct Led(());
		impl Led {
			fn new() -> Option<Self> {
				if mem::swap(&mut LED_EXISTS, true) {
					None
				} else {
					Some(Led(()))
				}
			}
		}
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{\currentname}
	
	\textbf{Step 2}: Ownership

	Not thread safe?

	\begin{lstlisting}
	mod peripherals {
		static LED_EXISTS : AtomicBool = AtomicBool::new(false);
		struct Led(());
		impl Led {
			fn new() -> Option<Self> {
				if LED_EXISTS.swap(true, Ordering::SeqCst) {
					None
				} else {
					Some(Led(()))
				}
			}
		}
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{\currentname}
Gets compiled to:
	\begin{lstlisting}
	movt	r0, #8192
	dmb		sy
	ldrexb	r2, [r0]
	strexb	r2, r1, [r0]
	cmp		r2, #0
	bne		#-14
	movw	r0, #14384
	dmb		sy
	\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	
	\textbf{Step 3}: Profit!

	\small\texttt{https://gitlab.com/gdox/embedded-rust/tree/master/olimex-led2}
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	
	\textbf{Step 4}: Lament the low-level approach and instead browse crates.io:

	\begin{itemize}
		\item svd2rust
		\item cortex-m-rt
		\item stm32
		\item stm32-hal
		\item ...
	\end{itemize}

	stm32 is auto-generated from SVD files: no mistakes.
\end{frame}

\section{Bonus: MISRA}
\makecurrentsection

\begin{frame}
	\frametitle{\currentname}
	
	Motor Industry Software Reliability Association

	Produces specifications for safe C and C++

	Most prominent low-level faults:
	\begin{itemize}
		\item Weird variable naming/clashing
		\item Invalid string handling
		\item Memory management issues
		\item Primitive type size mismatches
		\item \textbf{Integer promotion}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	\textit{A char, a short, or an int bit-field, or their signed or unsigned varieties, or an enumeration type, may be used in an expression wherever an int or unsigned int may be used. If a int can represent all values of the original type, the value is converted to an int; otherwise, it is converted to an unsigned int. These are called the integral promotions. All other arithmetic types are unchanged by the integral promotions.}

	- ANSI C specification
\end{frame}

\begin{frame}[fragile]
	\frametitle{\currentname}
	
	\begin{lstlisting}
	uint8_t average(uint8_t a, uint8_t b) {
		return (a + b) / 2;
	}

	int main() {
		printf("Average: %i\n",
						average(250, 250));
	}
	\end{lstlisting}
\end{frame}
\begin{frame}[fragile]
	\frametitle{\currentname}
	\begin{lstlisting}
	error[E0277]: cannot add `i32` to `i16`
	 --> integers.rs:8:19
	  |
	8 |     let a = 1_i16 + 2_i32;
	  |                   ^ no implementation for `i16 + i32`
	  |
	  = help: the trait `std::ops::Add<i32>` is not implemented for `i16`
	\end{lstlisting}

\end{frame}



%%------------------------------------------------
%\begin{frame}[t]
%    \frametitle{\currentname}
%    \textbf{Simple frame / top aligned}
%    \\
%    This is the simplest frame you can make (aligned on top).
%\end{frame}
%
%\begin{frame}[b]
%    \frametitle{\currentname}
%    \textbf{Simple frame / bottom aligned}
%    \\
%    This is the simplest frame you can make (aligned on bottom).
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Nested lists}
%    \begin{itemize}
%        \item first level
%        \begin{itemize}
%            \item subitem
%            \item subitem
%        \end{itemize}
%        \item first level
%        \begin{itemize}
%            \item subitem
%            \item subitem
%        \end{itemize}
%    \end{itemize}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Definition blocks}
%    \begin{definition}
%        A \alert{prime number} is a number that has exactly two divisors.
%    \end{definition}
%    \begin{example}
%        \begin{itemize}
%            \item 2 is prime (divisors: 1 and 2)
%            \item 3 is prime (divisors: 1 and 3)
%            \item 4 is \alert{not} a prime (divisors: 1, 2, and 4)
%        \end{itemize}
%    \end{example}
%    \begin{alertblock}{Watch out!}
%        \begin{itemize}
%            \item Prime numbers don't bite!
%        \end{itemize}
%    \end{alertblock}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Blocks with arbitrary titles}
%    \begin{block}{Answered questions}
%        How many primes are there?
%    \end{block}
%    \begin{block}{Open questions}
%        Is every number the sum of two primes ?
%    \end{block}
%    \textbf{Headless block}
%    \begin{block}{}
%        This box should have no title
%    \end{block}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{2 columns / center aligned}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item left item
%                \begin{itemize}
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item right item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{2 columns / top aligned}
%    \begin{columns}[onlytextwidth, t]
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item left item
%                \begin{itemize}
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item right item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{3 columns}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.33\textwidth}
%            \begin{itemize}
%                \item left item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.33\textwidth}
%            \begin{itemize}
%                \item center item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.33\textwidth}
%            \begin{itemize}
%                \item right item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Arrays}
%    \begin{itemize}
%        \item example table
%        \begin{tabular}{ l c c c }
%          1st column    & 2nd column    & 3rd column    & 4th column \\
%          \hline
%          first line    & \textbf{yes}  & \textbf{yes}  & \textbf{yes} \\
%          second line   & no            & \textbf{yes}  & \textbf{yes} \\
%          third line    & no            & no            & \textbf{yes} \\
%          \hline
%        \end{tabular}
%    \end{itemize}
%\end{frame}
%
%%------------------------------------------------
%
%\section{Text + pictures}
%\makecurrentsection
%
%%------------------------------------------------
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Full image / centered}
%    \begin{figure}
%        \includegraphics[width=50mm]{placeholder.png}
%        \caption{Centered with caption}
%    \end{figure}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Full image / centered no caption}
%    \begin{figure}
%        \includegraphics[width=50mm]{placeholder.png}
%    \end{figure}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Full image / left aligned}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%            \begin{figure}
%                \includegraphics[width=50mm]{placeholder.png}
%                \caption{Left aligned}
%            \end{figure}
%        \end{column}
%        \begin{column}{.5\textwidth}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Full image / right aligned}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \begin{figure}
%                \includegraphics[width=50mm]{placeholder.png}
%                \caption{Right aligned}
%            \end{figure}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Text on top + image on bottom}
%    \begin{itemize}
%        \item item
%        \begin{itemize}
%            \item subitem
%            \item subitem
%        \end{itemize}
%    \end{itemize}
%    \begin{figure}
%        \includegraphics[width=50mm]{placeholder.png}
%    \end{figure}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Image on top + text on bottom}
%    \begin{figure}
%        \includegraphics[width=50mm]{placeholder.png}
%    \end{figure}
%    \begin{itemize}
%        \item item
%        \begin{itemize}
%            \item subitem
%            \item subitem
%        \end{itemize}
%    \end{itemize}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Text on left + image on right}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                    \begin{itemize}
%                        \item subsubitem
%                    \end{itemize}
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \includegraphics[width=50mm]{placeholder.png}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Text on right + image on left}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%            \includegraphics[width=50mm]{placeholder.png}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                    \begin{itemize}
%                        \item subsubitem
%                    \end{itemize}
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Text on top + Text on left + image on right}
%    \begin{itemize}
%        \item item that should span above the image if everything is fine
%        \item item that should span above the image if everything is fine
%    \end{itemize}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                    \begin{itemize}
%                        \item subsubitem
%                    \end{itemize}
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \includegraphics[width=50mm]{placeholder.png}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Text on left + image on right + test on bottom}
%    \begin{columns}[onlytextwidth]
%        \begin{column}{.5\textwidth}
%            \begin{itemize}
%                \item item
%                \begin{itemize}
%                    \item subitem
%                    \item subitem
%                    \begin{itemize}
%                        \item subsubitem
%                    \end{itemize}
%                \end{itemize}
%            \end{itemize}
%        \end{column}
%        \begin{column}{.5\textwidth}
%            \includegraphics[width=50mm]{placeholder.png}
%        \end{column}
%    \end{columns}
%    \begin{itemize}
%        \item item that should span below the image if everything is fine
%        \item item that should span below the image if everything is fine
%    \end{itemize}
%\end{frame}
%
%%------------------------------------------------
%
%\section{Diagrams}
%\makecurrentsection
%
%%------------------------------------------------
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Class diagram}
%    \begin{center}
%        \begin{tikzpicture}[align=center, node distance=1.4cm and .5cm]
%            \node(hypotrochoid)[diagabstract, rectangle split, rectangle split parts=2]{
%                \textbf{hypotrochoid}
%                \nodepart{second}inner\_radius, outer\_radius, distance
%            };
%            \node(aux01)[below=of hypotrochoid]{};
%            \node(hypocycloid)[diagabstract, rectangle split, rectangle split parts=2, left=of aux01]{
%                \textbf{hypocycloid}
%                \nodepart{second}distance = inner\_radius
%            };
%            \node(ellipse)[diagabstract, rectangle split, rectangle split parts=2, right=of aux01]{
%                \textbf{ellipse}
%                \nodepart{second}outer\_radius = 2 * inner\_radius
%            };
%            \node(aux02)[below=of ellipse]{};
%            \node(circle)[diagabstract, rectangle split, rectangle split parts=2, right=of aux02]{
%                \textbf{circle}
%                \nodepart{second}distance = 0
%            };
%
%            \draw[diagarrow] (hypocycloid.north) -- ++(0,0.3) -| (hypotrochoid.south);
%            \draw[diagline] (hypocycloid.north) -- ++(0,0.3) -| (ellipse.north);
%            \draw[diagarrow] (circle.north) -- ++(0,0.5) -| (ellipse.south);
%        \end{tikzpicture}
%    \end{center}
%\end{frame}
%
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Venn diagram}
%    \begin{center}
%        \begin{tikzpicture}
%            \begin{scope}[blend group=overlay]
%                \fill[nred-50] (90:1.2) circle (2);
%                \fill[ngreen-50] (210:1.2) circle (2);
%                \fill[nlblue-50] (330:1.2) circle (2);
%            \end{scope}
%            \node at (90:2) {Penguins};
%            \node at (210:2) {Pizzas};
%            \node at (330:2) {Coffee};
%            \node[font=\Large] {Nalys};
%        \end{tikzpicture}
%    \end{center}
%\end{frame}
%
%%------------------------------------------------
%
%\section{Code}
%\makecurrentsection
%
%%------------------------------------------------
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Simple code snippets}
%    \begin{itemize}
%        \item simple code that does not need to be put into an external source file
%        \begin{lstlisting}
%int dummy()
%{
%    // do nothing
%    return -1;
%}
%        \end{lstlisting}
%    \end{itemize}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Simple code snippets / with escaped formating}
%        \begin{lstlisting}[language=bash]
%# should print hello
%<%\bfseries echo hello%>
%<%\itshape hello%>
%        \end{lstlisting}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Elements of code within text}
%    \begin{itemize}
%        \item code can be inlined like this \lstinline{void main(void)} to make use of syntax highlighting
%        \item also use that format when talking about operators like \lstinline{==} or \lstinline{+=}, otherwise they will be formatted by latex to look nicer within a text environment (-- != \lstinline{--})
%    \end{itemize}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%	\textbf{Code from file (code\_01.c)}
%    %\lstinputlisting{code/code_01.c}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\currentname}
%    \textbf{Code from file with item list (code\_01.c)}
%    \begin{itemize}
%        \item item
%		%\lstinputlisting{code/code_01.c}
%    \end{itemize}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Code from file / tiny (code\_02.c)}
%    %\lstinputlisting[style=tiny]{code/code_02.c}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\currentname}
%    \textbf{Code from file / startline and lastline (code\_03.c)}
%    %\lstinputlisting[firstline=4,lastline=7]{code/code_03.c}
%\end{frame}
