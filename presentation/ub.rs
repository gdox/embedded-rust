fn f(ptr : &i32) {
	let j : &mut i32 = unsafe{transmute(ptr)};
	*j = 6;
}

fn main() {
	let i = 5;
	f(&i);
	println!("{}", i); //prints 5. Or 6.
}
