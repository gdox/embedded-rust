#include <iostream>
void f(const int &i) {
	int &j = const_cast<int&>(i);
	j = 6;
}

int main() {
	int i = 5;
	f(i);
	std::cout << i << "\n"; //prints 6
}
