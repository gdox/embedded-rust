.PHONY: clean build-all package $(SUBDIRS)
build-all: $(SUBDIRS)

$(SUBDIRS):
	$(DOCKER_RUN) $(MK) -C $@

package:
	@for dir in $(SUBDIRS); do \
		$(MK) package -C $$dir; \
	done

clean:
	@if [ -d build ]; then \
		cd build && rm *; \
	fi
	@for dir in $(SUBDIRS); do \
		$(MK) clean -C $$dir; \
	done

