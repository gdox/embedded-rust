# Olimex-led

## Description

Olimex-led contains the necessary code to create a blinking led on an olimex stm32f405 board. 

## Usage

### Requirements

rustup, openocd

### Preparation

    rustup target add thumbv7m-none-eabi

### Building
    cargo build --release
    mv target/thumbv7m-none-eabi/release/olimex-led olimex-led.elf

### Flashing

    openocd -f interface/ftdi/olimex-arm-usb-ocd-h.cfg -f target/stm32f4x.cfg -c "init" -c "reset halt" -c "flash write_image erase olimex-led.elf" -c "reset" -c "shutdown"

## TODO
