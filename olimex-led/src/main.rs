#![no_main]
#![no_std]

use core::panic::PanicInfo;

unsafe fn modify<T : Copy, F : FnOnce(T) -> T>(t : *mut T, f : F) {
    let value = t.read_volatile();
    let result = f(value);
    t.write_volatile(result);
}

const GPIOC : usize = 0x40020800;
const GPIOC_MODE : *mut u32 = GPIOC as *mut u32;
const GPIOC_OUTPUT_TYPE : *mut u32 = (GPIOC + 0x4) as *mut u32;
const GPIOC_OUTPUT_SPEED: *mut u32 = (GPIOC + 0x8) as *mut u32;
const GPIOC_OUTPUT_DATA: *mut u32 = (GPIOC + 0x14) as *mut u32;

const RCC : usize = 0x40023800;
const RCC_AHB1_CLOCK : *mut u32 = (RCC + 0x30) as *mut u32;


unsafe fn led_init() {
    modify(RCC_AHB1_CLOCK, |w| {w | 0x4});
    modify(GPIOC_MODE, |w| {w | (1 << 24)});
    modify(GPIOC_OUTPUT_SPEED, |w| {w | (3 << 24)});
    modify(GPIOC_OUTPUT_TYPE, |w| {w | (1 << 12)});
}

fn hash_int(t : u64) -> u8 {
    let a = t as u8;
    let a = (a << 4) + (a >> 4);
    let a = ((a & 0xCC) >> 2) + ((a & 0x33) << 2);
    let a = ((a & 0xAA) >> 1) + ((a & 0x55) << 1);
    a
}


// The reset handler
#[no_mangle]
pub unsafe extern "C" fn Reset() -> ! {

    led_init();
    
    // can't return so we go into an infinite loop here
    let slowness = 5;
    let mut s : u64 = 1 << slowness;
    loop {
        for i in 0..s {
            let q = hash_int(i) as u64;
            if q * s * s > 255 * i * i {
                modify(GPIOC_OUTPUT_DATA, |w| {w | (1 << 12)});
            } else {
                modify(GPIOC_OUTPUT_DATA, |w| {w & !(1 << 12)});
            }
        }
        for i in (0..s).rev() {
            let q = hash_int(i) as u64;
            if q * s * s > 255 * i * i {
                modify(GPIOC_OUTPUT_DATA, |w| {w | (1 << 12)});
            } else {
                modify(GPIOC_OUTPUT_DATA, |w| {w & !(1 << 12)});
            }
        }
        s += s >> slowness;
    }
}

// The reset vector, a pointer into the reset handler
#[link_section = ".vector_table.reset_vector"]
#[no_mangle]
pub static RESET_VECTOR: unsafe extern "C" fn() -> ! = Reset;

#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}

